<!--
This file is part of DSGOV.BR - Web Components.

DSGOV.BR - Web Components is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
at your option) any later version.

DSGOV.BR - Web Components is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with DSGOV.BR - Web Components. If not, see <https://www.gnu.org/licenses/>6.

Copyright 2021 Serpro.
 -->

# DSGOV.BR - Web Components

Biblioteca de Web Components baseado no [Design System do Governo Federal Brasileiro](http://dsgov.estaleiro.serpro.gov.br/ 'Design System do Governo Federal Brasileiro').

## Objetivo

Construir uma biblioteca de componentes que faça uso das tecnologias de Web Components e que seja reutilizável em vários frameworks, bibliotecas e browsers.

## Tecnologias

Esse projeto é desenvolvido usando:

-   [VueJS 3](https://v3.vuejs.org/ 'VueJS 3')
-   [Storybook](https://storybook.js.org/ 'Storybook')
-   [Jest](https://jestjs.io/ 'Jest')

Para saber mais detalhes sobre Web Components sugerimos que consulte o [MDN](https://developer.mozilla.org/pt-BR/docs/Web/Web_Components 'Web Components | MDN').

## Como usar nossa biblioteca em seu projeto?

### Requisitos

Para conseguir usar nossa biblioteca sem problemas temos as seguintes dependências:

1. [DSGOV.BR](https://dsgov.estaleiro.serpro.gov.br/ 'Design System do Governo Federal Brasileiro')

2. [Font Awesome](https://fontawesome.com/ 'Font Awesome')

3. [Fonte Rawline](https://www.cdnfonts.com/rawline.font/ 'Fonte Rawline')

OBS: Consulte a documentação desses projetos para descobrir como usa-los no seu projeto.

### Instalação da biblioteca

Você pode instalar a nossa biblioteca de Web Components no seu projeto usando:

#### npm

```node
npm install --save @dsgovbr/webcomponents
```

#### yarn

```node
yarn add @dsgovbr/webcomponents
```

### Uso

Depois de instalada, importe a biblioteca de dentro da parta node_modules

```javascript
@import "node_modules/@dsgovbr/webcomponents/dist/webcomponents.umd.min.js"
```

## Documentação

A nossa documentação está disponível em duas versões:

### Estável

Essa é a versão que recomendamos o uso. É a nossa versão já testada e pronta para uso.

Acesse o [storybook](https://dsgov-br.gitlab.io/dsgov.br-webcomponents/main/ 'DSGOV.BR Web Components - Versão estável') e confira!

### Em desenvolvimento

Essa é a versão que estamos construindo. Devido a natureza do projeto estamos constantemente atualizando essa documentação com as novas features, componentes e correções de bugs.

Não recomendamos essa versão para uso em produção.

Quando essa versão é considerada pronta para produção ela é promovida para "estável" e começamos a trabalhar na próxima versão .

Acesse o [storybook](https://dsgov-br.gitlab.io/dsgov.br-webcomponents/main/ 'DSGOV.BR Web Components - Versão estável') e confira!

## Como executar o projeto?

Para executar a nossa biblioteca a partir do código-fonte você tem 2 opções:

###  Executar como VueJS

Nessa visualização você executa o projeto em uma página auxiliar (fora do storybook) e sem compilar o resultado para Web Components.

```node
npm run serve
```

###  Executar como Web Components

Nessa visualização o projeto é executado a partir do storybook e convertido para Web Components.

Aqui você consegue testar as configurações, obter snippets de código e visualizar os componentes rodando como nos ambientes de produção.

```node
npm run storybook
```

Essa compilação pode demorar para refletir as alterações na página. Assim, recomendamos que utilizem primeiro a execução como VueJS durante o desenvolvimento e quando considerarem as alterações maduras o suficiente façam os testes como Web Components.

##  Compilar biblioteca

Gera o build da biblioteca dentro da pasta **dist**.

```node
npm run build
```

##  Testes unitários

Executa os testes unitários e sava os resultados na pasta coverage.

```node
npm run test:unit
```

##  Lints

Nesse projeto usamos diversos tipos de lints para automaticamente verificar o código antes de enviar para review. Para executá-los e obter os resultados execute o comando:

```node
npm run lint
```

## Precisa de ajuda?

> Por favor **não** crie issues para fazer perguntas...

Use nossos canais abaixo para obter tirar suas dúvidas:

-   Site do DSGOV.BR <http://dsgov.estaleiro.serpro.gov.br/>

-   Web Components (versão estável) <https://dsgov-br.gitlab.io/dsgov.br-webcomponents/main>

-   Web Components (versão em desenvolvimento\*) <https://dsgov-br.gitlab.io/dsgov.br-webcomponents/develop>

-   Pelo nosso email <dsgov@serpro.gov.br>

-   Usando nosso canal no discord <https://discord.gg/U5GwPfqhUP>

\* Versões em desenvolvimento não são recomendadas para uso em produção

## Como contribuir?

Por favor verifique nossos guias de [como contribuir](./CONTRIBUTING.md 'Como contribuir?').

### Commits

Nesse projeto usamos um padrão para branches e commits. Por favor observe a documentação na nossa [wiki](https://dsgov-br.gitlab.io/dsgov.br-wiki/ 'Wiki') para aprender sobre os nossos padrões.

## Créditos

Esse repositório e o pacote de Web Components foi criado pelo [Serpro](https://www.serpro.gov.br/ 'SERPRO | Serviço Federal de Processamento de Dados') e [Dataprev](https://www.dataprev.gov.br/ 'Dataprev | Empresa de Tecnologia e Informações da Previdência').

## Licença

Nesse projeto usamos a [GNU Lesser General Public License v3](https://www.gnu.org/licenses/lgpl-3.0.pt-br.html 'LGPL v3')
