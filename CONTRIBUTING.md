<!--
This file is part of DSGOV.BR - Web Components.

DSGOV.BR - Web Components is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
at your option) any later version.

DSGOV.BR - Web Components is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with DSGOV.BR - Web Components. If not, see <https://www.gnu.org/licenses/>6.

Copyright 2021 Serpro.
 -->
# Contribuindo

Os guias sobre como contribuir para o DSGOV.BR podem ser encontrados na nossa [Wiki](https://dsgov-br.gitlab.io/dsgov.br-wiki/comunidade/contribuindo-com-o-dsgovbr/).
