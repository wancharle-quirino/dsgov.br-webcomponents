/*
 * This file is part of DSGOV.BR - Web Components.
 *
 * DSGOV.BR - Web Components is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 *
 * DSGOV.BR - Web Components is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with DSGOV.BR - Web Components. If not, see <https://www.gnu.org/licenses/>6.
 *
 * Copyright 2021 Serpro.
 */
import '../dist/dsgov.br-webcomponents.css'
import '../dist/dsgov.br-webcomponents.umd.min.js'
import { templateSourceCode } from '../src/util/Utils'

export const parameters = {
  actions: { argTypesRegex: '^on[A-Z].*' },
  controls: {
    matchers: {
      color: /(background|color)$/i,
      date: /Date$/,
    },
  },
  docs: {
    transformSource(src, ctx) {
      const match = /\b("')?template\1:\s*`([^`]+)`/.exec(src)
      if (match) {
        const { args, argTypes } = ctx.parameters
        let transformedSrc = templateSourceCode(match[2], args, argTypes)
        return transformedSrc
          .replaceAll('${args.default}', args.default)
          .replaceAll('${args.header}', args.header)
          .replaceAll('${args.content}', args.content)
          .replaceAll('${args.body}', args.body)
          .replaceAll('${args.footer}', args.footer)
          .replaceAll('${args.categorias}', args.categorias)
          .replaceAll('${args.redesSociais}', args.redesSociais)
          .replaceAll('${args.logo}', args.logo)
          .replaceAll('${args.footerImagens}', args.footerImagens)
          .replaceAll('${args.slotTemplate}', args.slotTemplate)
          .replaceAll('${args.titleList}', args.titleList)
          .replaceAll('${args.menu}', args.menu)
          .replaceAll('${args.action}', args.action)
      }
      return src
    },
  },
}
