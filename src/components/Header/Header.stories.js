/*
 * This file is part of DSGOV.BR - Web Components.
 *
 * DSGOV.BR - Web Components is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 *
 * DSGOV.BR - Web Components is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with DSGOV.BR - Web Components. If not, see <https://www.gnu.org/licenses/>6.
 *
 * Copyright 2021 Serpro.
 */
import { kebabiseArgs } from '../../util/Utils.js'

import BrHeader from './Header.ce.vue'
import HeaderAction from './HeaderAction.ce.vue'
import HeaderActionSearch from './HeaderActionSearch.ce.vue'
import HeaderMenu from './HeaderMenu.ce.vue'
import HeaderSearch from './HeaderSearch.ce.vue'

const slotHeaderAction = `
  <div slot="headerAction">
    <br-header-action
      has-login
      label-login="Entrar"
      image-avatar-login="https://picsum.photos/id/823/400"
      title-links="Acesso Rápido"
      title-functions="Funcionalidades do Sistema"
      list-links="[
        {
          name: 'Link de acesso 1',
          url: '#'
        },
        {
          name: 'Link de acesso 2',
          url: '#'
        },
        {
          name: 'Link de acesso 3',
          url: '#'
        }
      ]"
      list-functions="[
        {
          icon: 'chart-bar',
          name: 'Funcionalidade 1',
          url: '#'
        },
        {
          icon: 'headset',
          name: 'Funcionalidade 2',
          url: '#'
        },
        {
          icon: 'comment',
          name: 'Funcionalidade 3',
          url: '#'
        }
      ]"
    >
    </br-header-action>
  </div>
`
export default {
  title: 'Dsgov/br-header',
  component: BrHeader,
  subcomponents: {
    'br-header-menu': HeaderMenu,
    'br-header-action': HeaderAction,
    'br-header-search': HeaderSearch,
    'br-header-action-search': HeaderActionSearch,
  },
  argTypes: {
    density: {
      control: { type: 'select', options: ['small', 'medium', 'large'] },
    },
    default: {
      description: '**[OBRIGATÓRIO]** Conteúdo que deve ser passado por slot.',
      defaultValue: slotHeaderAction,
      control: 'text',
    },
    isSticky: {
      control: 'boolean',
      defaultValue: false,
    },
  },
}

const Template = (args) => ({
  props: Object.keys(args),
  setup() {
    return { args: kebabiseArgs(args) }
  },
  template: `<br-header v-bind="args">
  ${args.default}
  </br-header>`,
})

export const Padrao = Template.bind({})
Padrao.args = {
  default: slotHeaderAction,
  image:
    'https://cdn.dsgovserprodesign.estaleiro.serpro.gov.br/design-system/images/logo-positive.png',
  signature: 'Assinatura da empresa',
  title: 'Título do Header',
  subtitle: 'Subtítulo do Header',
  hasMenu: true,
  hasSearch: true,
  placeholderSearch: 'Digite o texto da pesquisa',
  compact: false,
  labelSearch: 'O que você procura?',
}

export const DensitySmall = Template.bind({})
DensitySmall.args = {
  density: 'small',
  default: slotHeaderAction,
  image:
    'https://cdn.dsgovserprodesign.estaleiro.serpro.gov.br/design-system/images/logo-positive.png',
  signature: 'Assinatura da empresa',
  title: 'Título do Header',
  subtitle: 'Subtítulo do Header',
  hasMenu: true,
  hasSearch: true,
  placeholderSearch: 'Digite o texto da pesquisa',
  compact: false,
  labelSearch: 'O que você procura?',
}

export const DensityLarge = Template.bind({})
DensityLarge.args = {
  density: 'large',
  default: slotHeaderAction,
  image:
    'https://cdn.dsgovserprodesign.estaleiro.serpro.gov.br/design-system/images/logo-positive.png',
  signature: 'Assinatura da empresa',
  title: 'Título do Header',
  subtitle: 'Subtítulo do Header',
  hasMenu: true,
  hasSearch: true,
  placeholderSearch: 'Digite o texto da pesquisa',
  compact: false,
  labelSearch: 'O que você procura?',
}

export const ContainerFluid = Template.bind({})
ContainerFluid.args = {
  default: slotHeaderAction,
  image:
    'https://cdn.dsgovserprodesign.estaleiro.serpro.gov.br/design-system/images/logo-positive.png',
  signature: 'Assinatura da empresa',
  title: 'Título do Header',
  subtitle: 'Subtítulo do Header',
  hasMenu: true,
  hasSearch: true,
  containerFluid: true,
  placeholderSearch: 'Digite o texto da pesquisa',
  compact: false,
  labelSearch: 'O que você procura?',
}

export const Compacto = Template.bind({})
Compacto.args = {
  default: slotHeaderAction,
  image:
    'https://cdn.dsgovserprodesign.estaleiro.serpro.gov.br/design-system/images/logo-positive.png',
  signature: 'Assinatura da empresa',
  title: 'Título do Header',
  subtitle: 'Subtítulo do Header',
  hasMenu: true,
  hasSearch: true,
  placeholderSearch: 'Digite o texto da pesquisa',
  compact: true,
  labelSearch: 'O que você procura?',
}
