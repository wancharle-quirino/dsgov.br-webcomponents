/*
 * This file is part of DSGOV.BR - Web Components.
 *
 * DSGOV.BR - Web Components is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 *
 * DSGOV.BR - Web Components is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with DSGOV.BR - Web Components. If not, see <https://www.gnu.org/licenses/>6.
 *
 * Copyright 2021 Serpro.
 */
import { shallowMount } from '@vue/test-utils'
import BrHeader from './Header.ce.vue'
import BrHeaderAction from './HeaderAction.ce.vue'
import BrList from '../List/List.ce.vue'
import BrItem from '../Item/Item.ce.vue'
import BrButton from '../Button/Button.ce.vue'

describe('Header', () => {
  test('it renders Header component', () => {
    const wrapper = shallowMount(BrHeader)
    expect(wrapper.classes('br-header')).toBe(true)
  })

  test('it renders logo image and signature', () => {
    const link =
      'https://cdn.dsgovserprodesign.estaleiro.serpro.gov.br/design-system/images/logo-positive.png'
    const assinatura = 'Assinatura da Empresa'

    const wrapper = shallowMount(BrHeader, {
      props: {
        image: link,
        signature: assinatura,
      },
    })

    expect(wrapper.find("img[src='" + link + "']").exists()).toBe(true)
    expect(wrapper.find('.header-sign').exists()).toBe(true)
    expect(wrapper.element).toMatchSnapshot()
  })

  test('it renders Menu', async () => {
    const wrapper = shallowMount(BrHeader, {
      propsData: {
        hasMenu: true,
      },
    })
    expect(wrapper.find('.header-menu-trigger').exists()).toBe(true)
    expect(wrapper.element).toMatchSnapshot()
  })

  test('it renders Title and subtitle', async () => {
    const wrapper = shallowMount(BrHeader, {
      props: {
        title: 'Título do Header',
        subtitle: 'Subtítulo do Header',
      },
    })
    expect(wrapper.find('.header-title').exists()).toBe(true)
    expect(wrapper.find('.header-subtitle').exists()).toBe(true)
    expect(wrapper.element).toMatchSnapshot()
  })

  test('it renders BrHeadersearch', async () => {
    const wrapper = shallowMount(BrHeader, {
      propsData: {
        hasSearch: true,
      },
    })
    expect(wrapper.find('.header-search').exists()).toBe(true)
    expect(wrapper.element).toMatchSnapshot()
  })
})

describe('HeaderAction', () => {
  test('it renders Search Button in compact mode', () => {
    const wrapper = shallowMount(BrHeader, {
      props: {
        hasSearch: true,
        compact: true,
      },
      slots: {
        headerAction: `
        <div slot="headerAction">
          <br-header-action>
          </br-header-action>
        </div>
        `,
      },
      global: {
        stubs: {
          'br-header-action': BrHeaderAction,
        },
      },
    })

    expect(wrapper.find('.header-search-trigger').exists()).toBe(true)
    expect(wrapper.element).toMatchSnapshot()
  })

  //Verifica se botao login eh renderizado
  test('it renders Login button', async () => {
    const wrapper = shallowMount(BrHeader, {
      slots: {
        headerAction: `
      <div slot="headerAction">
        <br-header-action has-login label-login="Entrar">
        </br-header-action>
      </div>
      `,
      },
      global: {
        stubs: {
          'br-header-action': BrHeaderAction,
          'br-button': BrButton,
        },
      },
    })

    expect(wrapper.find('.header-login').exists()).toBe(true)
    expect(wrapper.text()).toMatch('Entrar')
    expect(wrapper.element).toMatchSnapshot()
  })

  /* Verifica as as funcionalidades sao renderizadas */
  test('it renders functionalities', async () => {
    const wrapper = shallowMount(BrHeader, {
      slots: {
        headerAction: `
      <div slot="headerAction">
        <br-header-action
        title-functions="Funcionalidades do Sistema"
        list-functions="[
          {
            icon: 'chart-bar',
            name: 'Funcionalidade 1',
            url: 'http://www.url'
          }
        ]"
        >
        </br-header-action>
      </div>
      `,
      },
      global: {
        stubs: {
          'br-header-action': BrHeaderAction,
          'br-button': BrButton,
          'br-list': BrList,
          'br-item': BrItem,
        },
      },
    })

    const titleClass = wrapper.find('.title')
    expect(titleClass.exists()).toBe(true)
    expect(titleClass.text().trim()).toBe('Funcionalidades do Sistema')

    expect(wrapper.find("a[href='http://www.url']").exists()).toBe(true)

    const funcClass = wrapper.find('.align-items-center.br-item')
    expect(funcClass.exists()).toBe(true)
    expect(funcClass.text().trim()).toBe('Funcionalidade 1')
    expect(wrapper.element).toMatchSnapshot()
  })

  /* Verifica se os links sao renderizados com devidos links*/
  test('it renders links', () => {
    const wrapper = shallowMount(BrHeader, {
      slots: {
        headerAction: `
      <div slot="headerAction">
        <br-header-action
        title-links="Acesso Rápido"
        list-links="[
        {
          name: 'Link de acesso 1',
          url: 'http://www.link'
        }
      ]"
        >
        </br-header-action>
      </div>
      `,
      },
      global: {
        stubs: {
          'br-header-action': BrHeaderAction,
          'br-list': BrList,
          'br-item': BrItem,
        },
      },
    })
    expect(wrapper.find("a[href='http://www.link']").exists()).toBe(true)
    const linkClass = wrapper.find('.br-item')
    expect(linkClass.exists()).toBe(true)
    expect(linkClass.text().trim()).toBe('Link de acesso 1')
    expect(wrapper.element).toMatchSnapshot()
  })

  test('set prop container-fluid', () => {
    const wrapper = shallowMount(BrHeader, {
      props: {
        containerFluid: true,
      },
    })
    expect(wrapper.find('.container-fluid').exists()).toBe(true)
  })
})
