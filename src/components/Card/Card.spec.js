/*
 * This file is part of DSGOV.BR - Web Components.
 *
 * DSGOV.BR - Web Components is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 *
 * DSGOV.BR - Web Components is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with DSGOV.BR - Web Components. If not, see <https://www.gnu.org/licenses/>6.
 *
 * Copyright 2021 Serpro.
 */
import { shallowMount } from '@vue/test-utils'

import Card from '../Card/Card.ce.vue'
import CardHeader from '../Card/CardHeader.ce.vue'
import CardContent from '../Card/CardContent.ce.vue'
import CardFooter from '../Card/CardFooter.ce.vue'

describe('Card', () => {
  test('Testa se a prop disable está sendo renderizada.', () => {
    const wrapper = shallowMount(Card, {
      propsData: {
        disable: true,
      },
    })

    expect(wrapper.find('.br-card').attributes('disable')).toBe('true')
  })

  test('Testa a renderização do slot header.', () => {
    const wrapper = shallowMount(Card, {
      slots: {
        header: `
            <br-card-header slot="header">
              <div class="d-flex">
                <br-avatar imgSrc="https://picsum.photos/id/823/400" />
                <div class="ml-3">
                  <p class="h5 text-primary-default mb-0">Maria Amorim</p>
                  <span>UX Designer</span>
                </div>
                <div class="ml-auto">
                  <button type="circle">
                    <span slot="button-icon">
                      <i class="fas fa-ellipsis-v" aria-hidden="true"></i>
                    </span>
                  </button>
                </div>
              </div>
            </br-card-header>
        `,
      },
    })

    expect(wrapper.findComponent(CardHeader))
    expect(wrapper.element).toMatchSnapshot()
  })

  test('Testa a renderização do slot content.', () => {
    const wrapper = shallowMount(Card, {
      slots: {
        content: `
          <br-card-content slot="content">
            <p>
              Lorem ipsum dolor sit, amet consectetur adipisicing elit.
              Tempore perferendis nam porro atque ex at, numquam non
              optio ab eveniet error vel ad exercitationem, earum et
              fugiat recusandae harum? Assumenda.
            </p>
        </br-card-content>
        `,
      },
    })
    expect(wrapper.findComponent(CardContent))
    expect(wrapper.element).toMatchSnapshot()
  })

  test('Testa a renderização do slot footer.', () => {
    const wrapper = shallowMount(Card, {
      slots: {
        footer: `
          <br-card-footer slot="footer">
            <div class="d-flex">
              <div>
                <button label="Button" />
              </div>
              <div class="ml-auto">
                <button type="circle">
                  <span slot="button-icon">
                    <i class="fas fa-share-alt" aria-hidden="true"></i>
                  </span>
                </button>
                <button type="circle">
                  <span slot="button-icon">
                    <i class="fas fa-heart" aria-hidden="true"></i>
                  </span>
                </button>
              </div>
            </div>
          </br-card-footer>
        `,
      },
    })

    expect(wrapper.findComponent(CardFooter))
    expect(wrapper.element).toMatchSnapshot()
  })
})
