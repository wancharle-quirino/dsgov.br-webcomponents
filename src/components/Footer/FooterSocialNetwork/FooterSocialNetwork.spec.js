/*
 * This file is part of DSGOV.BR - Web Components.
 *
 * DSGOV.BR - Web Components is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 *
 * DSGOV.BR - Web Components is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with DSGOV.BR - Web Components. If not, see <https://www.gnu.org/licenses/>6.
 *
 * Copyright 2021 Serpro.
 */
import { shallowMount } from '@vue/test-utils'
import BrFooterSocialNetwork from './FooterSocialNetwork.ce.vue'

describe('BrFooterSocialNetwork', () => {
  test('Testa se o uso da classe social-network está sendo renderizado.', () => {
    const wrapper = shallowMount(BrFooterSocialNetwork)
    expect(wrapper.find('.social-network').exists()).toBe(true)
  })

  test('Testa se a prop label está sendo renderizada.', () => {
    const textoHello = 'hello'
    const wrapper = shallowMount(BrFooterSocialNetwork, {
      propsData: { label: textoHello },
    })
    expect(wrapper.text()).toMatch(textoHello)
  })

  test('Testa se o slot está sendo carregado dentro do elemento de classe social-network.', () => {
    const wrapper = shallowMount(BrFooterSocialNetwork, {
      slots: {
        default: '<div id="teste"></div>',
      },
    })
    expect(wrapper.find('.social-network #teste').exists()).toBe(true)
  })
})
