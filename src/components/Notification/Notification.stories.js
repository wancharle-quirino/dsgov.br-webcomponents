/*
 * This file is part of DSGOV.BR - Web Components.
 *
 * DSGOV.BR - Web Components is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 *
 * DSGOV.BR - Web Components is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with DSGOV.BR - Web Components. If not, see <https://www.gnu.org/licenses/>6.
 *
 * Copyright 2021 Serpro.
 */
import BrNotification from './Notification.ce.vue'
import { kebabiseArgs } from '../../util/Utils.js'

const defaultSlotContent = `
  <br-tab>
    <br-tab-item title="Aba 1" icon="bullhorn">
      <br-list>
        <br-item>
          <br-button icon="heartbeat">Home</br-button>
        </br-item>
        <br-divider></br-divider>
        <br-item>
          <br-button icon="heartbeat">Endereços</br-button>
        </br-item>
        <br-divider></br-divider>
        <br-item>
          <br-button icon="heartbeat">Notificação número 3</br-button>
        </br-item>
      </br-list>
    </br-tab-item>
    <br-tab-item title="Aba 2" icon="address-card">
      <br-list>
        <br-item>
          <br-button>
            <span class="text-bold d-block">Título</span>
            <span class="text-medium mb-2 d-block">25 de out</span>
            <span class="d-block">
              Nostrud consequat culpa ex mollit aute. Ex ex veniam ea labore
              laboris duis duis elit. Ex aute dolor enim aute Lorem dolor.
              Duis labore ad anim culpa. Non aliqua excepteur sunt eiusmod
              ex consectetur ex esse laborum velit ut aute.
            </span>
          </br-button>
        </br-item>
        <br-divider></br-divider>
        <br-item>
          <br-button>
            <span class="text-bold d-block">Título</span>
            <span class="text-medium mb-2 d-block">25 de out</span>
            <span class="d-block">
              Nostrud consequat culpa ex mollit aute. Ex ex veniam ea labore
              laboris duis duis elit. Ex aute dolor enim aute Lorem dolor.
              Duis labore ad anim culpa. Non aliqua excepteur sunt eiusmod
              ex consectetur ex esse laborum velit ut aute.
            </span>
          </br-button>
        </br-item>
        <br-divider></br-divider>
        <br-item>
          <br-button>
            <span class="text-bold d-block">Título</span>
            <span class="text-medium mb-2 d-block">25 de out</span>
            <span class="d-block">
              Nostrud consequat culpa ex mollit aute. Ex ex veniam ea labore
              laboris duis duis elit. Ex aute dolor enim aute Lorem dolor.
              Duis labore ad anim culpa. Non aliqua excepteur sunt eiusmod
              ex consectetur ex esse laborum velit ut aute.
            </span>
          </br-button>
        </br-item>
      </br-list>
    </br-tab-item>
  </br-tab>
`

export default {
  title: 'Dsgov/br-notification',
  component: BrNotification,
  argTypes: {
    title: {
      defaultValue: 'Título da notificação',
    },
    subtitle: {
      defaultValue: 'Subtítulo da notificação',
    },
    default: {
      description:
        '**[OBRIGATÓRIO]** Conteúdo da notificação, que deve ser passado por slot.',
      defaultValue: defaultSlotContent,
      control: 'text',
      type: {
        required: true,
      },
    },
  },
}

const TemplateDefault = (args) => ({
  props: Object.keys(args),
  setup() {
    return { args: kebabiseArgs(args) }
  },
  template: `<br-notification v-bind="args">${args.default}</br-notification>`,
})

export const Simple = TemplateDefault.bind({})
