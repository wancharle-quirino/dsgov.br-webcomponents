/*
 * This file is part of DSGOV.BR - Web Components.
 *
 * DSGOV.BR - Web Components is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 *
 * DSGOV.BR - Web Components is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with DSGOV.BR - Web Components. If not, see <https://www.gnu.org/licenses/>6.
 *
 * Copyright 2021 Serpro.
 */
import { shallowMount } from '@vue/test-utils'
import BrNotification from './Notification.ce.vue'

describe('br-notification', () => {
  test('Testa se a classe br-notification está renderizando.', () => {
    const wrapper = shallowMount(BrNotification, {
      propsData: {
        title: 'required title',
      },
    })
    expect(wrapper.find('.br-notification').exists()).toBe(true)
  })

  test('Testa se a classe notification-header está renderizando.', () => {
    const wrapper = shallowMount(BrNotification, {
      propsData: {
        title: 'required title',
      },
    })
    expect(wrapper.find('.notification-header').exists()).toBe(true)
  })

  test('Testa se a classe notification-body está renderizando.', () => {
    const wrapper = shallowMount(BrNotification, {
      propsData: {
        title: 'required title',
      },
    })
    expect(wrapper.find('.notification-body').exists()).toBe(true)
  })

  test('Testa se a prop title é exibida.', () => {
    const titleExample = 'title example'
    const wrapper = shallowMount(BrNotification, {
      propsData: {
        title: titleExample,
      },
    })
    expect(wrapper.text()).toMatch(titleExample)
  })

  test('Testa se a prop subtitle é exibida.', () => {
    const subtitleExample = 'subtitle example'
    const wrapper = shallowMount(BrNotification, {
      propsData: {
        title: 'required title',
        subtitle: subtitleExample,
      },
    })
    expect(wrapper.text()).toMatch(subtitleExample)
    expect(wrapper.element).toMatchSnapshot()
  })
})
