/*
 * This file is part of DSGOV.BR - Web Components.
 *
 * DSGOV.BR - Web Components is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 *
 * DSGOV.BR - Web Components is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with DSGOV.BR - Web Components. If not, see <https://www.gnu.org/licenses/>6.
 *
 * Copyright 2021 Serpro.
 */
import BrMagicButton from './MagicButton.ce.vue'
import { kebabiseArgs } from '../../util/Utils.js'

export default {
  title: 'Dsgov/br-magic-button',
  component: BrMagicButton,
  argTypes: {
    density: {
      control: { type: 'select', options: ['small', 'medium', 'large'] },
    },
    label: {
      defaultValue: '',
    },
    icon: {
      defaultValue: '',
    },
    circle: {
      defaultValue: false,
    },
    ariaLabel: {
      defaultValue: '',
    },
  },
}

const Template = (args) => ({
  props: Object.keys(args),
  setup() {
    return { args: kebabiseArgs(args) }
  },
  template: `<br-magic-button v-bind="args"></br-magic-button>`,
})

export const base = Template.bind({})
base.args = {
  label: 'Label',
}

export const circle = Template.bind({})
circle.args = {
  icon: 'cart-plus',
  circle: true,
}
