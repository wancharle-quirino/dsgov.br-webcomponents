/*
 * This file is part of DSGOV.BR - Web Components.
 *
 * DSGOV.BR - Web Components is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 *
 * DSGOV.BR - Web Components is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with DSGOV.BR - Web Components. If not, see <https://www.gnu.org/licenses/>6.
 *
 * Copyright 2021 Serpro.
 */
import BrList from './List.ce.vue'
import { kebabiseArgs } from '../../util/Utils.js'

const slotDefault = `
  <br-item>Primeiro item</br-item>
  <br-item>Segundo item</br-item>
  <br-item>Terceiro item</br-item>
`

const slotDefaultGrouping = `
  <br-item hover="true" title="Grupo 1">
    <br-list>
      <div class="row align-items-center">
        <div class="col-auto">
          <i class="fas fa-heartbeat" aria-hidden="true"></i>
        </div>
        <div class="col">Sub-item</div>
        <div class="col-auto">META</div>
      </div>
    </br-list>
  </br-item>
  <br-item hover="true" title="Grupo 2">
    <br-list>  
    <div class="row align-items-center">
        <div class="col-auto">
          <i class="fas fa-heartbeat" aria-hidden="true"></i>
        </div>
        <div class="col">Sub-item</div>
        <div class="col-auto">META</div>
      </div>
    </br-list>
  </br-item>
`

const slotDefaultDataToggle = `
  <br-item title="Item 1">
    <br-list>
      <br-item>Subitem 1.1</br-item>
      <br-item>Subitem 1.2</br-item>
      <br-item>Subitem 1.3</br-item>
    </br-list>
  </br-item>
  <br-divider></br-divider>
  <br-item title="Item 2">
    <br-list>
      <br-item>Detalhes do item 2</br-item>
    </br-list>
  </br-item>
  <br-divider></br-divider>
  <br-item title="Item 3">
    <br-list> Mais detalhes sobre o item 3 </br-list>
  </br-item>
  <br-divider></br-divider>
  <br-item title="Item 4">
    <br-list> Mais detalhes sobre o item 4 </br-list>
  </br-item>
`

export default {
  title: 'Dsgov/br-list',
  component: BrList,
  argTypes: {
    title: {
      control: 'text',
      defaultValue: '',
    },
    horizontal: {
      control: 'boolean',
      defaultValue: false,
    },
    dataToggle: {
      defaultValue: false,
    },
    dataUnique: {
      defaultValue: false,
    },
    checkable: {
      defaultValue: false,
    },
    default: {
      control: 'text',
      description:
        '**[OBRIGATÓRIO]** Conteúdo da lista, que deve ser passado por slot. Preferencialmente, devem ser do tipo **<br-item>**.',
    },
  },
}

const TemplateDefault = (args) => ({
  props: Object.keys(args),
  setup() {
    return { args: kebabiseArgs(args) }
  },
  template: `<br-list v-bind="args">
    <br-item title="Item 1">
      <br-list>
        <br-item>Subitem 1.1</br-item>
        <br-item>Subitem 1.2</br-item>
        <br-item>Subitem 1.3</br-item>
      </br-list>
    </br-item>
    <br-divider></br-divider>
    <br-item title="Item 2">
      <br-list>
        <br-item>Detalhes do item 2</br-item>
      </br-list>
    </br-item>
    <br-divider></br-divider>
    <br-item title="Item 3">
      <br-list> Mais detalhes sobre o item 3 </br-list>
    </br-item>
    <br-divider></br-divider>
    <br-item title="Item 4">
      <br-list> Mais detalhes sobre o item 4 </br-list>
    </br-item>
  </br-list>`,
})

export const Vertical = TemplateDefault.bind({})
Vertical.args = {
  title: 'Lista vertical',
  default: slotDefault,
}

export const Horizontal = TemplateDefault.bind({})
Horizontal.args = {
  title: 'Lista horizontal',
  horizontal: true,
  default: slotDefault,
}

export const Grouping = TemplateDefault.bind({})
Grouping.args = {
  title: 'Lista com agrupamentos',
  dataToggle: true,
  default: slotDefaultGrouping,
}

export const DataToggleItems = TemplateDefault.bind({})
DataToggleItems.args = {
  title: 'Isto é um <br-list> dataToggle que contém vários <br-item>',
  dataToggle: true,
  default: slotDefaultDataToggle,
}

export const DataUniqueAndDataToggleItems = TemplateDefault.bind({})
DataUniqueAndDataToggleItems.args = {
  title:
    'Isto é um <br-list data-unique dataToggle> que contém vários <br-item>',
  dataUnique: true,
  dataToggle: true,
  default: slotDefaultDataToggle,
}

export const DataToggleHorizontal = TemplateDefault.bind({})
DataToggleHorizontal.args = {
  title: 'Isto é um <br-list horizontal> que contém vários <br-item>',
  horizontal: true,
  dataToggle: true,
  default: slotDefaultDataToggle,
}
