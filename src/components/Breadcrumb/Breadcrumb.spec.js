/*
 * This file is part of DSGOV.BR - Web Components.
 *
 * DSGOV.BR - Web Components is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 *
 * DSGOV.BR - Web Components is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with DSGOV.BR - Web Components. If not, see <https://www.gnu.org/licenses/>6.
 *
 * Copyright 2021 Serpro.
 */
import { shallowMount } from '@vue/test-utils'
import Breadcrumb from './Breadcrumb.ce.vue'
import Crumb from './Crumb.ce.vue'

describe('Breadcrumb', () => {
  const wrapper = shallowMount(Breadcrumb, {
    propsData: {
      label: 'Default',
    },
  })

  test('it renders Breadcrumb.', () => {
    expect(wrapper.find('.br-breadcrumb').exists()).toBe(true)
  })

  test("it renders aria-label'", () => {
    expect(wrapper.attributes('aria-label')).toBe('Default')
    expect(wrapper.element).toMatchSnapshot()
  })
})

describe('Crumb', () => {
  const slotCrumb = `
  <br-crumb label="Início" home></br-crumb>
  <br-crumb label="Funcionalidade X"></br-crumb>`

  const wrapperCrumb = shallowMount(Breadcrumb, {
    slots: {
      default: slotCrumb,
    },
    global: {
      stubs: {
        'br-crumb': Crumb,
      },
    },
  })

  test('it renders crumb', () => {
    expect(wrapperCrumb.find('.crumb').exists()).toBe(true)
  })

  test('has the expected html structure', () => {
    expect(wrapperCrumb.element).toMatchSnapshot()
  })
})
