/*
 * This file is part of DSGOV.BR - Web Components.
 *
 * DSGOV.BR - Web Components is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 *
 * DSGOV.BR - Web Components is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with DSGOV.BR - Web Components. If not, see <https://www.gnu.org/licenses/>6.
 *
 * Copyright 2021 Serpro.
 */
import BrInput from './Input.ce.vue'
import { kebabiseArgs } from '../../util/Utils.js'

export default {
  title: 'Dsgov/br-input',
  component: BrInput,
  parameters: {
    controls: { exclude: ['newclass'] },
  },
  argTypes: {
    label: {
      control: 'text',
      defaultValue: '',
    },
    labelInline: {
      control: 'text',
      defaultValue: '',
    },
    placeholder: {
      control: 'text',
      defaultValue: '',
    },
    density: {
      control: { type: 'select', options: ['small', 'medium', 'large'] },
    },
    state: {
      control: {
        type: 'select',
        options: ['info', 'warning', 'danger', 'success'],
      },
    },
    disabled: {
      control: 'boolean',
      defaultValue: false,
    },
    autofocus: {
      control: 'boolean',
      defaultValue: false,
    },
    icon: {
      control: 'text',
      defaultValue: '',
    },
    iconSign: {
      control: 'text',
      defaultValue: '',
    },
    ispassword: {
      defaultValue: false,
    },
    isHighlight: {
      defaultValue: false,
    },
    id: {
      defaultValue: '',
    },
  },
}

const Template = (args) => ({
  props: Object.keys(args),
  setup() {
    return { args: kebabiseArgs(args) }
  },
  template: `<br-input v-bind="args"></br-input>`,
})

export const base = Template.bind({})
base.args = {
  label: 'Label',
  placeholder: 'Placeholder',
  disabled: false,
  state: '',
}

export const isHighlight = Template.bind({})
isHighlight.args = {
  label: 'Label',
  placeholder: 'Placeholder',
  disabled: false,
  isHighlight: true,
  state: '',
}

export const danger = Template.bind({})
danger.args = {
  label: 'CPF',
  placeholder: 'CPF inexistente.',
  state: 'danger',
  disabled: false,
}

export const success = Template.bind({})
success.args = {
  label: 'Nome Completo',
  placeholder: 'Fulano Beltrano',
  state: 'success',
  disabled: false,
}

export const warning = Template.bind({})
warning.args = {
  label: 'CPF',
  placeholder: 'Digite somente números',
  state: 'warning',
  disabled: false,
}

export const info = Template.bind({})
info.args = {
  label: 'CPF',
  placeholder: 'Digite somente números',
  state: 'info',
  disabled: false,
}

export const isPassword = Template.bind({})
isPassword.args = {
  label: 'Senha',
  placeholder: 'Digite a senha de 8 a 11 dígitos',
  disabled: false,
  ispassword: true,
}

export const icon = Template.bind({})
icon.args = {
  label: 'Logar',
  placeholder: 'Digite seu usuário',
  disabled: false,
  icon: 'arrow-right',
}

export const iconSign = Template.bind({})
iconSign.args = {
  label: 'Label/Rótulo',
  placeholder: 'Placeholder',
  disabled: false,
  iconSign: 'edit',
}

export const iconAndIconSign = Template.bind({})
iconAndIconSign.args = {
  label: 'Label/Rótulo',
  placeholder: 'Placeholder',
  disabled: false,
  iconSign: 'user',
  icon: 'arrow-right',
}

export const labelInline = Template.bind({})
labelInline.args = {
  labelInline: 'Label/Rótulo',
  placeholder: 'Placeholder',
  disabled: false,
  iconSign: 'user',
  icon: 'arrow-right',
}
export const Disabled = Template.bind({})
Disabled.args = {
  label: 'Label/Rótulo',
  placeholder: 'Placeholder',
  disabled: true,
  iconSign: 'user',
  icon: 'arrow-right',
}
