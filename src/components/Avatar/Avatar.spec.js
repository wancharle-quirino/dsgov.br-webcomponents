/*
 * This file is part of DSGOV.BR - Web Components.
 *
 * DSGOV.BR - Web Components is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 *
 * DSGOV.BR - Web Components is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with DSGOV.BR - Web Components. If not, see <https://www.gnu.org/licenses/>6.
 *
 * Copyright 2021 Serpro.
 */
import { shallowMount } from '@vue/test-utils'
import BrAvatar from './Avatar.ce.vue'
import FontAwesomeIcon from '../../font-awesome-share'

describe('BrAvatar', () => {
  test('Deve verificar se o tipo do avatar é Letra.', () => {
    const textName = 'Jhon Doe'
    const wrapper = shallowMount(BrAvatar, {
      propsData: {
        name: textName,
      },
    })
    expect(wrapper.text()).toMatch('J')
  })

  test("Deve verificar se o tipo do avatar é iconico'.", () => {
    const typeAvatar = true
    const wrapper = shallowMount(BrAvatar, {
      propsData: {
        iconic: typeAvatar,
      },
      global: {
        stubs: {
          'font-awesome-icon': FontAwesomeIcon,
        },
      },
    })
    expect(wrapper.find('.fa-user').exists()).toBe(true)
  })

  test("Deve verificar se o tipo do avatar é fotográfico'.", () => {
    const urlImageAvatar = 'https://picsum.photos/id/823/400'
    const wrapper = shallowMount(BrAvatar, {
      propsData: {
        image: urlImageAvatar,
      },
    })
    expect(wrapper.find("img[src='" + urlImageAvatar + "']").exists()).toBe(
      true
    )
  })

  test('Deve verificar se a densidade aplica é small', () => {
    const densitySmall = 'small'
    const wrapper = shallowMount(BrAvatar, {
      propsData: {
        name: 'Jhon Doe',
        density: densitySmall,
      },
    })
    expect(wrapper.find('span.br-avatar.small').exists()).toBe(true)
  })

  test('Deve verificar se a densidade aplica é medium', () => {
    const densityMedium = 'medium'
    const wrapper = shallowMount(BrAvatar, {
      propsData: {
        name: 'Jhon Doe',
        density: densityMedium,
      },
    })
    expect(wrapper.find('span.br-avatar.medium').exists()).toBe(true)
  })

  test('Deve verificar se a densidade aplica é large', () => {
    const densitylarge = 'large'
    const wrapper = shallowMount(BrAvatar, {
      propsData: {
        name: 'Jhon Doe',
        density: densitylarge,
      },
    })
    expect(wrapper.find('span.br-avatar.large').exists()).toBe(true)
  })

  test('has the expected html structure', () => {
    const wrapper = shallowMount(BrAvatar, {
      propsData: {
        name: 'Jhon Doe',
      },
    })
    expect(wrapper.element).toMatchSnapshot()
  })
})
