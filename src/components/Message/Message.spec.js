/*
 * This file is part of DSGOV.BR - Web Components.
 *
 * DSGOV.BR - Web Components is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 *
 * DSGOV.BR - Web Components is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with DSGOV.BR - Web Components. If not, see <https://www.gnu.org/licenses/>6.
 *
 * Copyright 2021 Serpro.
 */
import { shallowMount } from '@vue/test-utils'
import Message from './Message.ce.vue'

describe('br-message', () => {
  test('checks if the message is feedback type', () => {
    const wrapper = shallowMount(Message, {
      propsData: {
        feedback: true,
      },
    })
    expect(wrapper.find('.feedback').exists()).toBe(true)
  })

  test('assign state danger and check if it is true', () => {
    const wrapper = shallowMount(Message, {
      propsData: {
        state: 'danger',
      },
    })
    expect(wrapper.find('.br-message.danger').exists()).toBe(true)
  })

  test('assign state danger property and check if it is true', () => {
    const wrapper = shallowMount(Message, {
      propsData: {
        state: 'danger',
      },
    })
    expect(wrapper.find('.br-message.danger').exists()).toBe(true)
  })

  test('assign and checks if title property exists in a message', () => {
    const titulo = 'Titulo da mensagem'
    const wrapper = shallowMount(Message, {
      propsData: {
        title: titulo,
      },
    })
    expect(wrapper.text()).toMatch(titulo)
  })

  test('checks if title property do NOT exists in feedback', () => {
    const titulo = 'Titulo da mensagem'
    const wrapper = shallowMount(Message, {
      propsData: {
        feedback: true,
        title: titulo,
      },
    })
    expect(wrapper.find('strong').exists()).toBe(false)
  })

  test('assign inline property to title and check if it is true', () => {
    const titulo = 'Titulo da mensagem'
    const wrapper = shallowMount(Message, {
      propsData: {
        title: titulo,
        inline: true,
      },
    })
    expect(wrapper.find('p').exists()).toBe(false)
  })

  test('assign inverted property to message and check if it is true', () => {
    const wrapper = shallowMount(Message, {
      propsData: {
        inverted: true,
      },
    })
    expect(wrapper.find('.br-message.inverted').exists()).toBe(true)
  })

  test('assign closable property to message and check if it is true', () => {
    const wrapper = shallowMount(Message, {
      propsData: {
        closable: true,
      },
    })
    expect(wrapper.find('.close').exists()).toBe(true)
  })

  test('assign closable property to feedback and check if it do NOT exists', () => {
    const wrapper = shallowMount(Message, {
      propsData: {
        feedback: true,
        closable: true,
      },
    })
    expect(wrapper.find('.close').exists()).toBe(false)
  })

  test('emit dismiss method on closable message', () => {
    const wrapper = shallowMount(Message, {
      propsData: {
        closable: true,
      },
    })
    wrapper.vm.$emit('dismiss')
    expect(wrapper.emitted().dismiss).toBeTruthy()
  })

  test('message has the expected html structure - snapshot', () => {
    const titulo = 'Titulo da mensagem'
    const wrapper = shallowMount(Message, {
      propsData: {
        closable: true,
        title: titulo,
      },
      slots: {
        default: 'Texto da mensagem.',
      },
    })
    expect(wrapper.element).toMatchSnapshot()
  })

  test('feedback has the expected html structure - snapshot', () => {
    const wrapper = shallowMount(Message, {
      propsData: {
        feedback: true,
      },
      slots: {
        default: 'Texto da mensagem.',
      },
    })
    expect(wrapper.element).toMatchSnapshot()
  })
})
