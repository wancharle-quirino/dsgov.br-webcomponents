/*
 * This file is part of DSGOV.BR - Web Components.
 *
 * DSGOV.BR - Web Components is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 *
 * DSGOV.BR - Web Components is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with DSGOV.BR - Web Components. If not, see <https://www.gnu.org/licenses/>6.
 *
 * Copyright 2021 Serpro.
 */
import BrItem from './Item.ce.vue'
import { kebabiseArgs } from '../../util/Utils.js'

const defaultSimpleText = `Um texto simples`

const defaultImageAndText = `
  <div class="row align-items-center">
    <div class="col-auto"><img class="rounded" src="https://picsum.photos/40/40" alt="imagem de exemplo 1" /></div>
    <div class="col"><span>Item 1 - uma imagem e um texto (sem elemento de formulário)</span></div>
  </div>
`

export default {
  title: 'Dsgov/br-item',
  component: BrItem,
  argTypes: {
    active: {
      control: 'boolean',
      defaultValue: false,
    },
    hover: {
      control: 'boolean',
      defaultValue: false,
    },
    disabled: {
      control: 'boolean',
      defaultValue: false,
    },
    selected: {
      control: 'boolean',
      defaultValue: false,
    },
    'toggle-selected': {
      description:
        '**[OPCIONAL]** Evento disparado quando a prop selected é alterada.',
    },
    'toggle-open': {
      description:
        '**[OPCIONAL]** Evento disparado quando a prop open é alterada.',
    },
    default: {
      control: 'text',
      defaultValue: 'Texto do item',
      description:
        '**[OBRIGATÓRIO]** Conteúdo do item, que deve ser passado por slot.',
    },
    open: {
      defaultValue: false,
    },
    /**
     * **[OPCIONAL]** Título visível para o item quando o br-list pai é data-toggle.
     * */
    title: {
      defaultValue: '',
    },
    /**
     * **[OPCIONAL]** URL ou PATH pelo qual o usuario sera encaminhado ao clicar no item. Adicionar um link torna o item do tipo link.
     * */
    href: {
      defaultValue: '',
    },
    /**
     * **[OPCIONAL]** Indica que o item e um cabecalho de uma lista collapsavel. Deve se usar com title e com um br-list abaixo do
     * */
    header: {
      defaultValue: false,
    },
  },
}

const TemplateDefault = (args) => ({
  props: Object.keys(args),
  setup() {
    return { args: kebabiseArgs(args) }
  },
  template: `<br-item v-bind="args">${args.default}</br-item>`,
})

export const SimpleText = TemplateDefault.bind({})
SimpleText.args = {
  default: defaultSimpleText + '.',
  hover: true,
}

export const Disabled = TemplateDefault.bind({})
Disabled.args = {
  default: defaultSimpleText + ' desabilitado.',
  disabled: true,
}

export const ImageAndText = TemplateDefault.bind({})
ImageAndText.args = {
  default: defaultImageAndText,
  hover: true,
}

const TemplateMultipleCheckboxes = (args) => ({
  props: Object.keys(args),
  setup() {
    return { args: kebabiseArgs(args) }
  },
  template: `
  <div class="container">
    <div class="row">
      <div class="col-sm">
        <br-item hover onclick="document.getElementById('check-01').setAttribute('checked', this.checked = !this.checked)">
          <br-checkbox id="check-01" label="Item 1">
          </br-checkbox>
        </br-item>
      </div>
      <br-divider vertical/>
      <div class="col-sm">
        <br-item hover onclick="document.getElementById('check-02').setAttribute('checked', this.checked = !this.checked)">
          <br-checkbox id="check-02" label="Item 2">
          </br-checkbox>
        </br-item>
      </div>
    </div>
    <br-divider/>
    <div class="row">
      <div class="col-sm">
        <br-item hover onclick="document.getElementById('check-03').setAttribute('checked', this.checked = !this.checked)">
          <br-checkbox id="check-03" label="Item 3">
          </br-checkbox>
        </br-item>
      </div>
      <br-divider vertical/>
      <div class="col-sm">
        <br-item hover onclick="document.getElementById('check-04').setAttribute('checked', this.checked = !this.checked)">
          <br-checkbox checked id="check-04" label="Item 4">
          </br-checkbox>
        </br-item>
      </div>
    </div>
    <br-divider/>
  </div>
  `,
})

export const MultipleCheckboxes = TemplateMultipleCheckboxes.bind({})
MultipleCheckboxes.args = {}
MultipleCheckboxes.parameters = {
  controls: {
    disable: true,
  },
}
