/*
 * This file is part of DSGOV.BR - Web Components.
 *
 * DSGOV.BR - Web Components is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 *
 * DSGOV.BR - Web Components is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with DSGOV.BR - Web Components. If not, see <https://www.gnu.org/licenses/>6.
 *
 * Copyright 2021 Serpro.
 */
import { shallowMount } from '@vue/test-utils'
import Loading from './Loading.ce.vue'

describe('br-loading', () => {
  test('Testa se o componente com a barra de progresso esta renderizando.', () => {
    const wrapper = shallowMount(Loading, {
      propsData: {
        progress: true,
        percent: 80,
      },
    })
    expect(wrapper.find('.br-loading-mask').exists()).toBe(true)
    expect(wrapper.attributes('data-progress')).toBe('80')
    expect(wrapper.element).toMatchSnapshot()
  })

  test('Testa se o componente esta renderizando o loading circular do tipo medium', () => {
    const wrapper = shallowMount(Loading, {
      propsData: { medium: true },
    })
    expect(wrapper.find('.loading').exists()).toBe(true)
    expect(wrapper.find('.medium').exists()).toBe(true)
    expect(wrapper.element).toMatchSnapshot()
  })

  test('Testa se o componente esta renderizando o loading circular do tipo pequeno', () => {
    const wrapper = shallowMount(Loading)
    expect(wrapper.find('.loading').exists()).toBe(true)
    expect(wrapper.find('.medium').exists()).toBe(false)
    expect(wrapper.element).toMatchSnapshot()
  })

  test('Testa se o componente esta renderizando o label do loading circular', () => {
    const wrapper = shallowMount(Loading, {
      propsData: { rotulo: 'Carregando ...' },
    })
    expect(wrapper.find('.loading').exists()).toBe(true)
    expect(wrapper.find('.rotulo').exists()).toBe(true)
    expect(wrapper.element).toMatchSnapshot()
  })
})
