/*
 * This file is part of DSGOV.BR - Web Components.
 *
 * DSGOV.BR - Web Components is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 *
 * DSGOV.BR - Web Components is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with DSGOV.BR - Web Components. If not, see <https://www.gnu.org/licenses/>6.
 *
 * Copyright 2021 Serpro.
 */
import { shallowMount } from '@vue/test-utils'
import Checkbox from './Checkbox.ce.vue'

describe('Checkbox', () => {
  test('it renders br-checkbox', () => {
    const wrapper = shallowMount(Checkbox)
    expect(wrapper.classes('br-checkbox')).toBe(true)
  })

  test('set label attribute', () => {
    const rotulo = 'Label do checkbox'
    const wrapper = shallowMount(Checkbox, {
      propsData: {
        label: rotulo,
      },
    })
    expect(wrapper.text()).toMatch(rotulo)
  })

  test('set inline attribute', () => {
    const wrapper = shallowMount(Checkbox, {
      propsData: {
        inline: true,
      },
    })
    expect(wrapper.find('.br-checkbox.d-inline').exists()).toBe(true)
  })

  test('set disabled attribute', () => {
    const wrapper = shallowMount(Checkbox, {
      propsData: {
        disabled: true,
      },
    })
    expect(wrapper.props('disabled')).toBe(true)
  })

  test('set format attribute = valid', () => {
    const wrapper = shallowMount(Checkbox, {
      propsData: {
        format: "valid",
      },
    })
    expect(wrapper.find('.br-checkbox.valid').exists()).toBe(true)
  })

  test('set format attribute = invalid', () => {
    const wrapper = shallowMount(Checkbox, {
      propsData: {
        format: "invalid",
      },
    })
    expect(wrapper.find('.br-checkbox.invalid').exists()).toBe(true)
  })

  test('set indeterminate checkbox', () => {
    const wrapper = shallowMount(Checkbox, {
      propsData: {
        indeterminate: true,
      },
    })
    expect(wrapper.props('indeterminate')).toBe(true)
  })

  test('set checked checkbox', () => {
    const wrapper = shallowMount(Checkbox, {
      propsData: {
        checked: true,
      },
    })
    expect(wrapper.props('checked')).toBe(true)
  })

  test('has the expected html structure', () => {
    const wrapper = shallowMount(Checkbox)
    expect(wrapper.element).toMatchSnapshot()
  })
})
