/*
 * This file is part of DSGOV.BR - Web Components.
 *
 * DSGOV.BR - Web Components is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 *
 * DSGOV.BR - Web Components is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with DSGOV.BR - Web Components. If not, see <https://www.gnu.org/licenses/>6.
 *
 * Copyright 2021 Serpro.
 */
const kebabise = (string) => {
  const upper =
    /(?<!\p{Uppercase_Letter})\p{Uppercase_Letter}|\p{Uppercase_Letter}(?!\p{Uppercase_Letter})/gu
  return string.replace(upper, '-$&').replace(/^-/, '').toLowerCase()
}

export const kebabiseArgs = (args) => {
  const argsKebabCase = {}
  Object.keys(args)
    .filter(
      (key) =>
        ![
          'default',
          'header',
          'content',
          'body',
          'footer',
          'categorias',
          'redesSociais',
          'logo',
          'footerImagens',
          'slotTemplate',
        ].includes(key)
    )
    .forEach((key) => {
      argsKebabCase[kebabise(key)] =
        typeof args[key] === 'boolean' && !args[key] ? null : args[key]
    })
  return argsKebabCase
}

export const templateSourceCode = (
  templateSource,
  args,
  argTypes,
  replacing = 'v-bind="args"'
) => {
  const componentArgs = {}
  for (const [k, t] of Object.entries(argTypes)) {
    const val = args[k]
    if (
      typeof val !== 'undefined' &&
      !(typeof val === 'boolean' && val === false) &&
      t.table &&
      t.table.category === 'props' &&
      val !== t.defaultValue
    ) {
      componentArgs[k] = val
    }
  }

  const propToSource = (key, val) => {
    const type = typeof val
    switch (type) {
      case 'boolean':
        return val ? kebabise(key) : ''
      default:
        return `${kebabise(key)}="${val}"`
    }
  }

  const propsAndValuesArray = Object.keys(componentArgs)
    .filter(
      (key) =>
        ![
          'default',
          'header',
          'content',
          'body',
          'footer',
          'categorias',
          'redesSociais',
          'logo',
          'footerImagens',
          'slotTemplate',
        ].includes(key)
    )
    .map((key) => propToSource(key, args[key]))

  return templateSource.replaceAll(
    propsAndValuesArray.length ? replacing : ' ' + replacing,
    propsAndValuesArray.join(' ')
  )
}

export const parsePropStringToJSON = (propString) =>
  JSON.parse(propString.replaceAll(/(\w+):\s/gi, '"$1": ').replaceAll("'", '"'))
