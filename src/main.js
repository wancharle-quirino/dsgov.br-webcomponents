/*
 * This file is part of DSGOV.BR - Web Components.
 *
 * DSGOV.BR - Web Components is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 *
 * DSGOV.BR - Web Components is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with DSGOV.BR - Web Components. If not, see <https://www.gnu.org/licenses/>6.
 *
 * Copyright 2021 Serpro.
 */
import '@govbr/dsgov/dist/dsgov-base.css'
import { defineCustomElement } from 'vue'
import Avatar from './components/Avatar/Avatar.ce.vue'
import BrBreadcrumb from './components/Breadcrumb/Breadcrumb.ce.vue'
import BrCrumb from './components/Breadcrumb/Crumb.ce.vue'
import Button from './components/Button/Button.ce.vue'
import BrCard from './components/Card/Card.ce.vue'
import BrCardContent from './components/Card/CardContent.ce.vue'
import BrCardFooter from './components/Card/CardFooter.ce.vue'
import BrCardHeader from './components/Card/CardHeader.ce.vue'
import BrCheckbox from './components/Checkbox/Checkbox.ce.vue'
import Divider from './components/Divider/Divider.ce.vue'
import BrFooter from './components/Footer/Footer.ce.vue'
import BrFooterImage from './components/Footer/FooterImage/FooterImage.ce.vue'
import BrFooterLogo from './components/Footer/FooterLogo/FooterLogo.ce.vue'
import BrFooterSocialNetwork from './components/Footer/FooterSocialNetwork/FooterSocialNetwork.ce.vue'
import BrFooterSocialNetworkItem from './components/Footer/FooterSocialNetworkItem/FooterSocialNetworkItem.ce.vue'
import BrHeader from './components/Header/Header.ce.vue'
import BrHeaderAction from './components/Header/HeaderAction.ce.vue'
import BrHeaderSearch from './components/Header/HeaderSearch.ce.vue'
import BrInput from './components/Input/Input.ce.vue'
import BrItem from './components/Item/Item.ce.vue'
import BrList from './components/List/List.ce.vue'
import BrLoading from './components/Loading/Loading.ce.vue'
import MagicButton from './components/MagicButton/MagicButton.ce.vue'
import BrMenu from './components/Menu/Menu.ce.vue'
import Message from './components/Message/Message.ce.vue'
import BrNotification from './components/Notification/Notification.ce.vue'
import BrSwitch from './components/Switch/Switch.ce.vue'
import Tab from './components/Tab/Tab.ce.vue'
import TabItem from './components/Tab/TabItem.ce.vue'

const componentsConfig = [
  { tagName: 'br-avatar', component: Avatar },
  { tagName: 'br-button', component: Button },
  { tagName: 'br-magic-button', component: MagicButton },
  { tagName: 'br-card', component: BrCard },
  { tagName: 'br-checkbox', component: BrCheckbox },
  { tagName: 'br-card-header', component: BrCardHeader },
  { tagName: 'br-card-content', component: BrCardContent },
  { tagName: 'br-card-footer', component: BrCardFooter },
  { tagName: 'br-divider', component: Divider },
  { tagName: 'br-input', component: BrInput },
  { tagName: 'br-tab', component: Tab },
  { tagName: 'br-loading', component: BrLoading },
  { tagName: 'br-tab-item', component: TabItem },
  { tagName: 'br-list', component: BrList },
  { tagName: 'br-item', component: BrItem },
  { tagName: 'br-switch', component: BrSwitch },
  { tagName: 'br-breadcrumb', component: BrBreadcrumb },
  { tagName: 'br-crumb', component: BrCrumb },
  { tagName: 'br-message', component: Message },
  { tagName: 'br-footer', component: BrFooter },
  { tagName: 'br-footer-logo', component: BrFooterLogo },
  { tagName: 'br-footer-image', component: BrFooterImage },
  { tagName: 'br-footer-social-network', component: BrFooterSocialNetwork },
  {
    tagName: 'br-footer-social-network-item',
    component: BrFooterSocialNetworkItem,
  },
  { tagName: 'br-header', component: BrHeader },
  { tagName: 'br-header-action', component: BrHeaderAction },
  { tagName: 'br-header-search', component: BrHeaderSearch },
  { tagName: 'br-notification', component: BrNotification },
  { tagName: 'br-menu', component: BrMenu },
]

componentsConfig.forEach((config) => {
  customElements.define(config.tagName, defineCustomElement(config.component))
})
